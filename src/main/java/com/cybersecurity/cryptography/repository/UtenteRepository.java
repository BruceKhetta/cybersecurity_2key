package com.cybersecurity.cryptography.repository;

import com.cybersecurity.cryptography.dao.UtenteDao;
import com.cybersecurity.cryptography.dto.UtenteDto;
import org.hibernate.loader.collection.OneToManyJoinWalker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UtenteRepository extends JpaRepository<UtenteDao, Integer> {
    @Query(nativeQuery = true, value = "select * from utente where email = ?1")
    UtenteDao login(String email);
}
