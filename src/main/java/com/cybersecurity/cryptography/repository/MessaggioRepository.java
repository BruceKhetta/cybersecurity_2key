package com.cybersecurity.cryptography.repository;

import com.cybersecurity.cryptography.dao.MessaggioDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessaggioRepository extends JpaRepository<MessaggioDao, Integer> {
}
