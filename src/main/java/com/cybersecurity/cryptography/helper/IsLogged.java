package com.cybersecurity.cryptography.helper;

import com.cybersecurity.cryptography.dto.UtenteDto;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;

import io.jsonwebtoken.*;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;

public class IsLogged {
    private static final long TTL= 30000;
    private static final String SECRET_KEY = "ahvbhaj24n2k4nlknn42l4n4n6776bb6bnbb4mn4mnr";

    public static String createToken(UtenteDto utente){
        long nowMillis = System.currentTimeMillis();

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        JwtBuilder builder = Jwts.builder()
                .setId(utente.getEmail())
                .setExpiration(new Date(TTL + nowMillis));

        return builder.compact();
    }

    public static boolean decodeToken(String token){
        try{
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                    .parseClaimsJwt(token).getBody();
            return true;
        } catch (ExpiredJwtException e) {
             return false;
        }
    }

    public static boolean checkToken(String token){
        return false;
    }
}
