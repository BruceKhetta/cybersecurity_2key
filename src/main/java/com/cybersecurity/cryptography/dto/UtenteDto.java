package com.cybersecurity.cryptography.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.security.PublicKey;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UtenteDto {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "cellulare")
    private long cellulare;

    @Column(name = "codice_fiscale")
    private String codice_fiscale;

    private PublicKey chiave_pubblica;
}
