package com.cybersecurity.cryptography.sevice;

import com.cybersecurity.cryptography.dao.UtenteDao;
import com.cybersecurity.cryptography.dto.LoginUtenteDto;
import com.cybersecurity.cryptography.dto.UtenteDto;

import java.security.PrivateKey;

public interface UtenteService {
    boolean newUser(UtenteDao utente);
    String login(LoginUtenteDto utente);
    PrivateKey getKey(LoginUtenteDto utente);
}
