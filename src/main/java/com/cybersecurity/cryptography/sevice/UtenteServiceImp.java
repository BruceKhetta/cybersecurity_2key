package com.cybersecurity.cryptography.sevice;

import com.cybersecurity.cryptography.dao.UtenteDao;
import com.cybersecurity.cryptography.dto.LoginUtenteDto;
import com.cybersecurity.cryptography.dto.UtenteDto;
import com.cybersecurity.cryptography.helper.Cryptography;
import com.cybersecurity.cryptography.helper.IsLogged;
import com.cybersecurity.cryptography.repository.UtenteRepository;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.PrivateKey;

@Service
@Transactional
public class UtenteServiceImp implements UtenteService {
    String salt = "5vzt18kwHC";

    @Autowired
    UtenteRepository utenteRepository;

    @Override
    public PrivateKey getKey(LoginUtenteDto utente) {
        return null;
    }

    private UtenteDto daoToDto(UtenteDao dao){
        UtenteDto dto = new UtenteDto();

        dto.setCodice_fiscale(dao.getCodice_fiscale());
        dto.setCognome(dao.getCognome());
        dto.setEmail(dao.getEmail());
        dto.setId(dao.getId());
        dto.setNome(dao.getNome());
        dto.setCellulare(dao.getCellulare());

        return dto;
    }

    @Override
    public boolean newUser(UtenteDao utente) {
        String encPassword = Cryptography.generateSecurePassword(utente.getPassword(), salt);
        utente.setPassword(encPassword);
        this.utenteRepository.save(utente);
        return true;
    }

    @Override
    public String login(LoginUtenteDto utente) {
        UtenteDao dao = utenteRepository.login(utente.getEmail());
        if(Cryptography.verifyUserPassword(utente.getPassword(), dao.getPassword(), salt)) {
            UtenteDto dto = daoToDto(dao);
            return IsLogged.createToken(dto);
        }
        else return null;
    }
}