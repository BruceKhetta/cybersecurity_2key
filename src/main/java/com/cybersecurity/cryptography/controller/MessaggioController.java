package com.cybersecurity.cryptography.controller;

import com.cybersecurity.cryptography.dto.BaseResponseDto;
import com.cybersecurity.cryptography.dto.MessaggioDto;
import com.cybersecurity.cryptography.helper.IsLogged;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "api/messaggio")
public class MessaggioController {

    @PostMapping(value = "/new", produces = "application/json")
    public BaseResponseDto<MessaggioDto> newMessaggio(
            @RequestBody MessaggioDto messaggio,
            @RequestBody Map<String, String> token
    ){
        BaseResponseDto<MessaggioDto> responseDto = new BaseResponseDto<>();

        String jwt = token.get("token");
        if(!IsLogged.decodeToken(jwt)) {
            responseDto.setResponse("login");
            return responseDto;
        }

        

        return responseDto;
    }
}
