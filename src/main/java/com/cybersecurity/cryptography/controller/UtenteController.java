package com.cybersecurity.cryptography.controller;

import com.cybersecurity.cryptography.dao.UtenteDao;
import com.cybersecurity.cryptography.dto.BaseResponseDto;
import com.cybersecurity.cryptography.dto.LoginUtenteDto;
import com.cybersecurity.cryptography.dto.UtenteDto;
import com.cybersecurity.cryptography.helper.IsLogged;
import com.cybersecurity.cryptography.sevice.UtenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "api/utente")
public class UtenteController {
    @Autowired
    UtenteService utenteService;

    @PostMapping(produces = "application/json")
    public BaseResponseDto<UtenteDto> register(@RequestBody UtenteDao utente){
        BaseResponseDto<UtenteDto> responseDto = new BaseResponseDto<>();

        responseDto.setResponse(utenteService.newUser(utente));

        return responseDto;
    }

    @PostMapping(value = "/login", produces = "application/json")
    public BaseResponseDto<UtenteDto> login(@RequestBody LoginUtenteDto utente){
        BaseResponseDto<UtenteDto> responseDto = new BaseResponseDto<>();

        String token = utenteService.login(utente);
        if(token == null) responseDto.setResponse("Wrong user or password");
        else responseDto.setResponse(token);

        return responseDto;
    }

    @PostMapping(value = "/generatekey", produces = "application/json")
    public BaseResponseDto<Map<String, String>> getPrivateKey(@RequestBody Map<String, String> token){
        BaseResponseDto<Map<String, String>> responseDto = new BaseResponseDto<>();

        String jwt = token.get("token");
        if(!IsLogged.decodeToken(jwt)) {
            responseDto.setResponse("login");
            return responseDto;
        }

        return responseDto;
    }
}
