package com.cybersecurity.cryptography.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.security.PublicKey;

@Entity
@Table(name = "utente")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UtenteDao {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "cellulare")
    private long cellulare;

    @Column(name = "codice_fiscale")
    private String codice_fiscale;

    @Column(name = "indirizzo")
    private String indirizzo;

    @Column(name = "chiave_pubblica")
    private PublicKey chiave_pubblica;
}
