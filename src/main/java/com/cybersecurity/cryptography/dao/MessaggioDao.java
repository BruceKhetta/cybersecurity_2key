package com.cybersecurity.cryptography.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "messaggio")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessaggioDao {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "oggetto")
    private String oggetto;

    @Column(name = "corpo")
    private String corpo;

    @Column(name = "data")
    private Date data;
}
